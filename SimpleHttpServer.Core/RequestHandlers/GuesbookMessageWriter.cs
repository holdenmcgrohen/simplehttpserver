﻿using System;
using System.Collections.Specialized;
using SimpleHttpServer.Core.Guestbook;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal class GuesbookMessageWriter : GuestbookRequestHandler
    {
        public GuesbookMessageWriter(GuestbookRepositoryType repositoryType)
            : base(repositoryType)
        {
        }

        protected override string HttpMethod => "POST";

        public override ServerResponse GetResponse(NameValueCollection requestParams)
        {
            int userId = 0;
            if (!int.TryParse(requestParams["user"], out userId))
            {
                throw new InvalidOperationException("User ID not specified");
            }

            string message = requestParams["message"];
            if (string.IsNullOrEmpty(message))
            {
                throw new InvalidOperationException("Message not specified");
            }

            this.Repository.AddMessage(userId, message);

            return new ServerResponse("Message added");
        }
    }
}
