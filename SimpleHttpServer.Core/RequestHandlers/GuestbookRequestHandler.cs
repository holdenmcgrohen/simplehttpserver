﻿using SimpleHttpServer.Core.Guestbook;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal abstract class GuestbookRequestHandler : RequestHandler
    {
        public GuestbookRequestHandler(GuestbookRepositoryType repositoryType)
        {
            this.Repository = GuestbookRepository.Get(repositoryType);
        }

        protected GuestbookRepository Repository { get; private set; }

        protected override string UrlPattern => "/Guestbook/";
    }
}
