﻿using System.Collections.Specialized;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal class DefaultRequestHandler : RequestHandler
    {
        protected override string HttpMethod => "GET";

        protected override string UrlPattern => "/";

        public override ServerResponse GetResponse(NameValueCollection requestParams)
        {
            return new ServerResponse("Hello World!");
        }
    }
}
