﻿using System.Collections.Specialized;
using System.Text;
using SimpleHttpServer.Core.Guestbook;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal class GuesbookMessageReader : GuestbookRequestHandler
    {
        public GuesbookMessageReader(GuestbookRepositoryType repositoryType)
            : base(repositoryType)
        {
        }

        protected override string HttpMethod => "GET";

        public override ServerResponse GetResponse(NameValueCollection requestParams)
        {
            var messages = this.Repository.GetMessages();
            var html = new StringBuilder("<html><body>Message list<table callspacing=1 border=1><thead><tr><td>Id</td><td>User</td><td>Text</td></tr></thead>");
            foreach (var message in messages)
            {
                html.Append("<tr>");
                html.Append("<td>" + message.Id + "</td>");
                html.Append("<td>" + message.User.Name + "</td>");
                html.Append("<td>" + message.Text + "</td>");
                html.Append("</tr>");
            }
            html.Append("</table></body></html>");
            return new ServerResponse(html.ToString());
        }
    }
}
