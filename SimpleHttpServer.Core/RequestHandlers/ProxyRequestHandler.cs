﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal class ProxyRequestHandler : RequestHandler
    {
        protected override string HttpMethod => "GET";

        protected override string UrlPattern => "/Proxy/";

        public override ServerResponse GetResponse(NameValueCollection requestParams)
        {
            Uri uri = null;
            if (!Uri.TryCreate(requestParams["url"], UriKind.Absolute, out uri))
            {
                throw new InvalidOperationException("URL not specified");
            }

            var request = WebRequest.Create(uri);
            request.Method = "GET";

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        responseStream.CopyTo(memoryStream);
                        var data = memoryStream.ToArray();
                        return new ServerResponse(data, response.ContentType, TryParseEncoding(response.ContentEncoding), response.StatusCode);
                    }
                }
            }
        }

        private static Encoding TryParseEncoding(string encodingName)
        {
            try
            {
                return Encoding.GetEncoding(encodingName);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
    }
}
