﻿using System;
using System.Collections.Specialized;

namespace SimpleHttpServer.Core.RequestHandlers
{
    internal abstract class RequestHandler
    {
        protected abstract string HttpMethod { get; }

        protected abstract string UrlPattern { get; }

        public abstract ServerResponse GetResponse(NameValueCollection requestParams);

        public bool CanHandleRequest(string requestMethod, Uri requestUrl)
        {
            return requestMethod == this.HttpMethod
                && 
                (requestUrl.AbsolutePath.Equals(this.UrlPattern, StringComparison.OrdinalIgnoreCase)
                || requestUrl.AbsolutePath.Equals(this.UrlPattern.TrimEnd('/'), StringComparison.OrdinalIgnoreCase));
        }
    }
}
