﻿using System.Net;
using System.Text;

namespace SimpleHttpServer.Core
{
    internal class ServerResponse
    {
        public ServerResponse(string responseText)
            : this(responseText, HttpStatusCode.OK)
        {
        }

        public ServerResponse(string responseText, HttpStatusCode statusCode)
        {
            this.ContentType = "text/html";
            this.ContentEncoding = Encoding.UTF8;
            this.Content = Encoding.UTF8.GetBytes(responseText);
            this.StatusCode = statusCode;
        }

        public ServerResponse(byte[] content, string contentType, Encoding contentEncoding, HttpStatusCode statusCode)
        {
            this.Content = content;
            this.ContentType = contentType;
            this.ContentEncoding = contentEncoding;
            this.StatusCode = statusCode;
        }

        public string ContentType { get; private set; }

        public Encoding ContentEncoding { get; private set; }

        public byte[] Content { get; private set; }

        public HttpStatusCode StatusCode { get; private set; }

        public override string ToString()
        {
            return (this.ContentEncoding ?? Encoding.UTF8).GetString(this.Content);
        }
    }
}
