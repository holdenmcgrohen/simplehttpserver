﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SimpleHttpServer.Core.RequestHandlers;
using SimpleHttpServer.Infrastructure;

namespace SimpleHttpServer.Core
{
    public class Server : IDisposable
    {
        private readonly HttpListener listener;
        private readonly ICollection<RequestHandler> handlers;
        private readonly ServerSettings settings;

        public Server(ServerSettings settings)
        {
            this.settings = settings;
            this.listener = new HttpListener();
            this.handlers =
                new RequestHandler[]
                {
                    new ProxyRequestHandler(),
                    new GuesbookMessageReader(this.settings.RepositoryType),
                    new GuesbookMessageWriter(this.settings.RepositoryType),
                    new DefaultRequestHandler()
                };

            this.listener.Prefixes.Add($"http://+:{this.settings.Port}/");
        }

        public void Start()
        {
            if (!this.listener.IsListening)
            {
                this.listener.Start();
                Task.Factory.StartNew(this.WaitForRequests, TaskCreationOptions.LongRunning);
                Logger.Info($"Local server started at port {this.settings.Port}");
            }
        }

        public void Stop()
        {
            if (this.listener.IsListening)
            {
                this.listener.Stop();
            }
        }

        public void Dispose()
        {
            if (this.listener != null)
            {
                ((IDisposable)this.listener).Dispose();
            }
        }

        private static async void WriteResponse(HttpListenerResponse response, ServerResponse responseContent)
        {
            response.StatusCode = (int)responseContent.StatusCode;
            response.ContentEncoding = responseContent.ContentEncoding;
            response.ContentType = responseContent.ContentType;

            await response.OutputStream.WriteAsync(responseContent.Content, 0, responseContent.Content.Length);
            response.OutputStream.Close();
            Logger.Info($"Sent response: HTTP {response.StatusCode}, length: {responseContent.Content.Length}");
        }

        private async void WaitForRequests()
        {
            while (this.listener.IsListening)
            {
                HttpListenerContext listenerContext = null;
                try
                {
                    listenerContext = await this.listener.GetContextAsync();
                    Logger.Info("Request received: " + listenerContext.Request.RawUrl);

                    var response = this.GetResponse(listenerContext.Request);
                    WriteResponse(listenerContext.Response, response);
                }
                catch (HttpListenerException) when (!this.listener.IsListening)
                {
                    return;
                }
                catch (Exception e)
                {
                    Logger.Error("Request handling failed: " + e.ToString());
                    if (listenerContext != null)
                    {
                        WriteResponse(listenerContext.Response, new ServerResponse("Error processing request", HttpStatusCode.InternalServerError));
                    }
                }
            }
        }

        private ServerResponse GetResponse(HttpListenerRequest request)
        {
            var handler = this.handlers.FirstOrDefault(h => h.CanHandleRequest(request.HttpMethod, request.Url));
            if (handler == null)
            {
                throw new InvalidOperationException("No suitable handlers found for request");
            }

            return handler.GetResponse(request.GetParameters());
        }
    }
}
