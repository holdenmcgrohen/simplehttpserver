﻿using SimpleHttpServer.Core.Guestbook;

namespace SimpleHttpServer.Core
{
    public class ServerSettings
    {
        public int Port { get; set; }

        public GuestbookRepositoryType RepositoryType { get; set; }
    }
}
