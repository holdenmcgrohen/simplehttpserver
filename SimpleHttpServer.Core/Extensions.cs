﻿using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;

namespace SimpleHttpServer.Core
{
    internal static class Extensions
    {
        internal static NameValueCollection GetParameters(this HttpListenerRequest request)
        {
            var result = request.QueryString;

            string body = null;
            if (request.HasEntityBody)
            {
                using (var inputStream = request.InputStream)
                {
                    using (var reader = new StreamReader(inputStream, request.ContentEncoding))
                    {
                        body = reader.ReadToEnd();
                    }
                }
            }

            if (body != null)
            {
                string[] rawParams = body.Split('&');
                foreach (string param in rawParams)
                {
                    string[] kvPair = param.Split('=');
                    if (kvPair.Length == 2)
                    {
                        string key = kvPair[0];
                        string value = HttpUtility.UrlDecode(kvPair[1]);
                        result.Add(key, value);
                    }
                }
            }

            return result;
        }
    }
}
