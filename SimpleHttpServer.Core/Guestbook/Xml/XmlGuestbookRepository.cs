﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace SimpleHttpServer.Core.Guestbook.Xml
{
    internal class XmlGuestbookRepository : GuestbookRepository
    {
        private readonly string fileName;

        public XmlGuestbookRepository(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            this.fileName = fileName;
        }

        public override void AddMessage(int user, string message)
        {
            var data = this.ReadXml();
            if (!data.Users.Any(u => u.Id == user))
            {
                throw new ArgumentException("Could not find user #" + user, nameof(user));
            }

            data.Messages.Add(new MessageEntry { Id = data.Messages.Max(m => m.Id) + 1, Text = message, UserId = user });
            using (var fileStream = File.OpenWrite(this.fileName))
            {
                new XmlSerializer(typeof(Data)).Serialize(fileStream, data);
            }
        }

        public override ICollection<GuestbookMessage> GetMessages()
        {
            var data = this.ReadXml();
            return data.ToGuestbookMessages();
        }

        private Data ReadXml()
        {
            using (var stringReader = File.OpenRead(this.fileName))
            {
                using (var xmlTextReader = new XmlTextReader(stringReader))
                {
                    return (Data)new XmlSerializer(typeof(Data)).Deserialize(xmlTextReader);
                }
            }
        }
    }

}
