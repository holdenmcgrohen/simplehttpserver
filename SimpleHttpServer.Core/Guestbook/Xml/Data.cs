﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleHttpServer.Core.Guestbook.Xml
{
    public class Data
    {
        public List<UserEntry> Users { get; set; }

        public List<MessageEntry> Messages { get; set; }

        public ICollection<GuestbookMessage> ToGuestbookMessages()
        {
            var users = this.Users.Select(u => new GuestbookUser { Id = u.Id, Name = u.Name });

            var result = new List<GuestbookMessage>();

            foreach (var xmlMessage in this.Messages)
            {
                var message = new GuestbookMessage
                {
                    Id = xmlMessage.Id,
                    Text = xmlMessage.Text,
                    User = users.Single(u => u.Id == xmlMessage.UserId)
                };
                result.Add(message);
            }

            return result;
        }
    }
}
