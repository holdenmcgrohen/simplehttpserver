﻿namespace SimpleHttpServer.Core.Guestbook.Xml
{
    public class MessageEntry
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int UserId { get; set; }
    }
}
