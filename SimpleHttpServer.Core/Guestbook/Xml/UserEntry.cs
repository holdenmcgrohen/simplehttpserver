﻿namespace SimpleHttpServer.Core.Guestbook.Xml
{
    public class UserEntry
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
