﻿namespace SimpleHttpServer.Core.Guestbook
{
    public class GuestbookMessage
    {
        public virtual int Id { get; set; }

        public virtual GuestbookUser User { get; set; }

        public virtual string Text { get; set; }
    }
}
