﻿namespace SimpleHttpServer.Core.Guestbook
{
    public enum GuestbookRepositoryType
    {
        XmlFile,
        SqLiteDatabase
    }
}
