﻿namespace SimpleHttpServer.Core.Guestbook
{
    public class GuestbookUser
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}
