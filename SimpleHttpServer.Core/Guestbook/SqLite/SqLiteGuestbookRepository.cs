﻿using System;
using System.Collections.Generic;
using SimpleHttpServer.Core.Guestbook.SqLite.NHibernate;

namespace SimpleHttpServer.Core.Guestbook.SqLite
{
    internal class SqLiteGuestbookRepository : GuestbookRepository
    {
        private readonly NHibernateRepository internalRepository;

        public SqLiteGuestbookRepository(string connectionString)
        {
            this.internalRepository = this.InitializeInternalRepository(connectionString);
        }

        public override void AddMessage(int user, string message)
        {
            using (var session = this.internalRepository.OpenSession())
            {
                var userEntry = session.Get<GuestbookUser>(user);

                if (userEntry == null)
                {
                    throw new ArgumentException("Could not find user #" + user, nameof(user));
                }

                var messageEntry = new GuestbookMessage { Text = message, User = userEntry };
                session.Save(messageEntry);
            }
        }

        public override ICollection<GuestbookMessage> GetMessages()
        {
            using (var session = this.internalRepository.OpenSession())
            {
                return session.QueryOver<GuestbookMessage>().JoinQueryOver(m => m.User).List();
            }
        }

        protected virtual NHibernateRepository InitializeInternalRepository(string connectionString)
        {
            return new NHibernateRepository(connectionString);
        }
    }
}
