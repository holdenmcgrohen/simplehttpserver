﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace SimpleHttpServer.Core.Guestbook.SqLite.NHibernate
{
    internal class ReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            instance.Column(instance.Property.Name + "Id");
        }
    }
}
