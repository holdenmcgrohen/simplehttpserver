﻿using System;
using System.Linq;
using FluentNHibernate.Automapping;

namespace SimpleHttpServer.Core.Guestbook.SqLite.NHibernate
{
    internal class AutomappingConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            return new[] { typeof(GuestbookMessage), typeof(GuestbookUser) }.Contains(type);
        }
    }
}
