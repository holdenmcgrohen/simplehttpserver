﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace SimpleHttpServer.Core.Guestbook.SqLite.NHibernate
{
    internal class NHibernateRepository
    {
        private readonly ISessionFactory sessionFactory;

        public NHibernateRepository(string connectionString)
        {
            var cfg = new AutomappingConfiguration();

            this.sessionFactory = Fluently.Configure()
              .Database(SQLiteConfiguration.Standard.ConnectionString(connectionString))
              .Mappings(m =>
                m.AutoMappings
                  .Add(
                    AutoMap.AssemblyOf<GuestbookRepository>(cfg)
                    .Conventions.Add<ReferenceConvention>()
                      .Override<GuestbookMessage>(map => map.Table("Messages"))
                      .Override<GuestbookMessage>(map => map.Map(x => x.Text).Not.Nullable().Length(256))
                      .Override<GuestbookUser>(map => map.Table("Users"))
                      .Override<GuestbookUser>(map => map.Map(x => x.Name).Not.Nullable().Length(256))
                  )
                )
              .BuildSessionFactory();
        }

        public ISession OpenSession()
        {
            return this.sessionFactory.OpenSession();
        }
    }
}
