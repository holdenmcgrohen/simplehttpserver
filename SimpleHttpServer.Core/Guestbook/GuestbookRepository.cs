﻿using System.Collections.Generic;
using System.ComponentModel;
using SimpleHttpServer.Core.Guestbook.SqLite;
using SimpleHttpServer.Core.Guestbook.Xml;
using SimpleHttpServer.Infrastructure;

namespace SimpleHttpServer.Core.Guestbook
{
    internal abstract class GuestbookRepository
    {
        internal static GuestbookRepository Get(GuestbookRepositoryType repositoryType)
        {
            switch (repositoryType)
            {
                case GuestbookRepositoryType.XmlFile:
                    return ServiceLocator.GetInstance<XmlGuestbookRepository>();
                case GuestbookRepositoryType.SqLiteDatabase:
                    return ServiceLocator.GetInstance<SqLiteGuestbookRepository>();
                default:
                    throw new InvalidEnumArgumentException(nameof(repositoryType), (int)repositoryType, repositoryType.GetType());
            }
        }

        public abstract ICollection<GuestbookMessage> GetMessages();

        public abstract void AddMessage(int user, string message);
    }
}
