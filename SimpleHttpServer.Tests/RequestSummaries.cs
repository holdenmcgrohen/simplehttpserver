﻿using System;
using System.Collections.Generic;

namespace SimpleHttpServer.Tests
{
    internal class RequestSummaries : List<Tuple<string, string>>
    {
        public void Add(string item, string item2)
        {
            this.Add(new Tuple<string, string>(item, item2));
        }
    }
}
