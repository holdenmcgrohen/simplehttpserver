﻿using System;
using System.Collections.Specialized;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleHttpServer.Core.Guestbook;
using SimpleHttpServer.Core.RequestHandlers;

namespace SimpleHttpServer.Tests
{
    [TestClass]
    public class GuestbookMessageWriterTests
    {
        private readonly GuesbookMessageWriter[] writers = new[]
        {
             new GuesbookMessageWriter(GuestbookRepositoryType.XmlFile),
             new GuesbookMessageWriter(GuestbookRepositoryType.SqLiteDatabase)
        };

        [TestMethod]
        public void TestUrls()
        {
            var validRequests = new RequestSummaries
            {
                { "POST", "Http://localhost/Guestbook" },
                { "POST", "Http://localhost:8811/guestbook" },
            };

            var invalidRequests = new RequestSummaries
            {
                { "GET", "Http://localhost/Guestbook" },
                { "PUT", "Http://localhost:11/guestbook" }
            };

            foreach (var reader in this.writers)
            {
                reader.ShouldBeAbleToHandleRequests(validRequests);
                reader.ShouldBeUnableToHandleRequests(invalidRequests);
            }
        }

        [TestMethod]
        public void TestSuccessfulResponse()
        {
            foreach (var writer in this.writers)
            {
                writer.GetResponse(new NameValueCollection { { "user", "0" }, { "message", "NewMessage" } }).ToString().ShouldEqual("Message added");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void FirstWriterFail()
        {
            this.writers[0].GetResponse(new NameValueCollection { { "user", "InvalidId" }, { "message", "NewMessage" } });
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SecondWriterFail()
        {
            this.writers[1].GetResponse(new NameValueCollection { { "user", "InvalidId" }, { "message", "NewMessage" } });
        }
    }
}
