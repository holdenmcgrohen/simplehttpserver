﻿using System.Collections.Generic;
using SimpleHttpServer.Core.Guestbook;
using SimpleHttpServer.Core.Guestbook.SqLite;
using SimpleHttpServer.Core.Guestbook.SqLite.NHibernate;

namespace SimpleHttpServer.Tests.Mocks
{
    internal class SqLiteGuestbookRepositoryMock : SqLiteGuestbookRepository
    {
        private static readonly object SyncLock = new object();
        private static readonly ICollection<GuestbookMessage> Messages = new List<GuestbookMessage>();

        public SqLiteGuestbookRepositoryMock()
            : base(string.Empty)
        {
            Messages.Add(new GuestbookMessage { Id = 0, Text = "MockMessage", User = new GuestbookUser { Id = 0, Name = "MockUser" } });
        }

        public override void AddMessage(int user, string message)
        {
            lock (SyncLock)
            {
                Messages.Add(new GuestbookMessage { User = new GuestbookUser { Id = user, Name = "Mock" }, Text = message });
            }
        }

        public override ICollection<GuestbookMessage> GetMessages()
        {
            return Messages;
        }

        protected override NHibernateRepository InitializeInternalRepository(string connectionString)
        {
            return null;
        }
    }
}
