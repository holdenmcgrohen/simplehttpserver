﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleHttpServer.Core.RequestHandlers;

namespace SimpleHttpServer.Tests
{
    internal static class Extensions
    {
        internal static void ShouldBeFalse(this bool condition, string message = null)
        {
            Assert.IsFalse(condition, message);
        }

        internal static void ShouldBeTrue(this bool condition, string message = null)
        {
            Assert.IsTrue(condition, message);
        }

        internal static object ShouldEqual(this object actual, object expected, string message = null)
        {
            Assert.AreEqual(expected, actual, message);
            return expected;
        }

        internal static void ShouldBeAbleToHandleRequests(this RequestHandler handler, RequestSummaries requests)
        {
            foreach (var request in requests)
            {
                handler.CanHandleRequest(request.Item1, new Uri(request.Item2)).ShouldBeTrue();
            }
        }

        internal static void ShouldBeUnableToHandleRequests(this RequestHandler handler, RequestSummaries requests)
        {
            foreach (var request in requests)
            {
                handler.CanHandleRequest(request.Item1, new Uri(request.Item2)).ShouldBeFalse();
            }
        }
    }
}
