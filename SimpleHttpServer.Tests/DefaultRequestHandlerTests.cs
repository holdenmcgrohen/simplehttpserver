﻿using System.Collections.Specialized;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleHttpServer.Core.RequestHandlers;

namespace SimpleHttpServer.Tests
{
    [TestClass]
    public class DefaultRequestHandlerTests
    {
        private readonly DefaultRequestHandler handler = new DefaultRequestHandler();

        [TestMethod]
        public void TestUrls()
        {
            var validRequests = new RequestSummaries
            {
                { "GET", "Http://localhost/" },
                { "GET", "Http://localhost:8811" },
            };
            this.handler.ShouldBeAbleToHandleRequests(validRequests);

            var invalidRequests = new RequestSummaries
            {
                { "GET", "Http://localhost/1" },
                { "PUT", "Http://localhost:8811" }
            };
            this.handler.ShouldBeUnableToHandleRequests(invalidRequests);
        }

        [TestMethod]
        public void TestResponse()
        {
            this.handler.GetResponse(new NameValueCollection()).ToString().ShouldEqual("Hello World!");
        }
    }
}
