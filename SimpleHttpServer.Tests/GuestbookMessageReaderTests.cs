﻿using System.Collections.Specialized;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleHttpServer.Core.Guestbook;
using SimpleHttpServer.Core.RequestHandlers;

namespace SimpleHttpServer.Tests
{
    [TestClass]
    public class GuestbookMessageReaderTests
    {
        private readonly GuesbookMessageReader[] readers = new[]
        {
             new GuesbookMessageReader(GuestbookRepositoryType.XmlFile),
             new GuesbookMessageReader(GuestbookRepositoryType.SqLiteDatabase)
        };

        [TestMethod]
        public void TestUrls()
        {
            var validRequests = new RequestSummaries
            {
                { "GET", "Http://localhost/Guestbook" },
                { "GET", "Http://localhost:8811/guestbook" },
            };

            var invalidRequests = new RequestSummaries
            {
                { "POST", "Http://localhost/Guestbook" },
                { "PUT", "Http://localhost:11/guestbook" }
            };
            foreach (var reader in this.readers)
            {
                reader.ShouldBeAbleToHandleRequests(validRequests);
                reader.ShouldBeUnableToHandleRequests(invalidRequests);
            }
        }

        [TestMethod]
        public void TestGetMessages()
        {
            const string expectedXml = "<html><body>Message list<table callspacing=1 border=1><thead><tr><td>Id</td><td>User</td><td>Text</td></tr></thead><tr><td>0</td><td>MockUser</td><td>MockMessage</td></tr></table></body></html>";
            foreach (var reader in this.readers)
            {
                reader.GetResponse(new NameValueCollection()).ToString().ShouldEqual(expectedXml);
            }
        }
    }
}
