﻿using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace SimpleHttpServer.Infrastructure
{
    public static class ServiceLocator
    {
        private static WindsorContainer container = new WindsorContainer(new XmlInterpreter());

        /// <summary>
        /// Получение экземпляра службы
        /// </summary>
        /// <typeparam name="T">Тип службы</typeparam>
        /// <returns>Экземпляр службы</returns>
        public static T GetInstance<T>()
        {
            return container.Resolve<T>();
        }
    }
}
