﻿using NLog;

namespace SimpleHttpServer.Infrastructure
{
    public static class Logger
    {
        private static readonly NLog.Logger InternalLogger = LogManager.GetCurrentClassLogger();

        public static void Info(string message)
        {
            InternalLogger.Info(message);
        }

        public static void Error(string message)
        {
            InternalLogger.Error(message);
        }
    }
}
