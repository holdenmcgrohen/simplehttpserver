﻿using System;
using SimpleHttpServer.Core;
using SimpleHttpServer.Infrastructure;

namespace SimpleHttpServer.ConsoleHost
{
    internal class Program
    {
        internal static void Main()
        {
            var settings = ServiceLocator.GetInstance<ServerSettings>();
            using (var server = new Server(settings))
            {
                Logger.Info("Starting");
                server.Start();
                Logger.Info("Press <ENTER> to stop");
                Console.ReadLine();
                server.Stop();
                Logger.Info("Stopped");
            }
        }
    }
}
