Simple HTTP server as a console application.

Functions:
1) Default response for "/" URL is �Hello world!�
2) Simple guestbook management features:
    - GET /Guestbook/ produces all guestbook records
	- POST /Guestbook/ adds a record to guestbook (accepts 2 parameters: user and message)
   Data can be stored in 2 ways: an XML file and in a SQLite database
3) Requet to "/Proxy/" URL produces the contents of the oage at the URL specified in the request parameter.

* Startup project - SimpleHttpServer.ConsoleHost.
* Guestbook data source configuration is stored in App.config (component id="ServerSettings")
* Tests project - SimpleHttpServer.Tests.





